"""Flask API with various endpoints."""
from flask import Flask, Response

app = Flask(__name__)

# @app.get('/')
# def redirect_to_info():
#     return get_info()


@app.get("/")
@app.get("/info")
def get_info() -> Response:
    """Return API info."""
    info = {
        "API": "my-api", 
        "Version": "0.0.1", 
        "Team": "OPS"
        }
    app.logger.debug("info=%s", info)
    return info


if __name__ == "__main__":
    app.run(debug=False, host="0.0.0.0", port=8080)
